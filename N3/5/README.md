# **Lerndokumentation N2/1**

## **Anforderungen**


| Produkt:   | Site **Global** | Site **Local** | 
|:-----------|:---------------:|:--------------:|
| PCs        | 30              | 8              | 
| Laptops    | 60              | 7              |
| Drucker    | 7               | 2              |
| Router     | 1               | 1
| **Total**  | **98**          | **18**         |


Danach habe ich die Netzwerke nach der Grösse nach geordnet und die Passende CIDR hinzugefügt.


| Netz | Benötigte Adressen | Gewählte  Netzgrösse |
|------|-----------|----------------|
| Netz A | 23 PCs + 20 LTs + 7 Dr + 1 RT + 2 = 53  |  64 |
| Netz B | 65 PCs + 55 LTs + 7 DR + 1 RT + 2 = 130 | 255 |
| Netz T | 0 PCs + 0 LTs + 0 DR + 2 RT + 2 = 4  |   4  |
| Netz I | 0 PCs + 0 LTs + 0 DR + 2 RT + 2 = 4  |   4  |



| Netz   | Grösse | Netzadresse/Netzmaske (Bit) | Dezimale Schreibweise der Netzmaske | Broadcastadresse |
|--------|-----|-----|-----|-----|
| Netz I |  4  | 95.2.50.100/30  | 255.255.255.252 | 95.2.50.103 |
| Netz T |  4  | 192.168.100.252/30   | 255.255.255.252 | 192.168.101.255 |
| Netz A |  64 | 192.168.101.0/26 | 255.255.255.192 | 192.168.101.63 |
| Netz B |  255 | 192.168.100.0/24 | 255.255.255.0 | 192.168.100.255 |



### **Router Konfiguration**

| **Router**      | **Interfaceadresse** | **Interface** |
|-----------------|----------------------|---------------|
| Router A Netz I | 95.2.50.10230    | s1            |
| Router A Netz A | 192.168.101.1/26       | e0            |
| Router A Netz T | 192.168.101.253/30     | s0            |
| Router B Netz B | 192.168.100.1/24     | e0            |
| Router B Netz T | 192.168.101.254/30     | s0            |


### **Geräte Konfiiguration**

| -       | IP-Addresse      | Gateway       | Subnetzmaske    | 
| ------- | ---------------- | ------------- | --------------- |
| PC0     | 192.168.101.2    | 192.168.100.2      | 255.255.255.128 |
| Laptop0 | 192.168.101.3    | 192.168.100.3      | 255.255.255.128 |
| PC1     | 192.168.100.2    | 192.168.100.1    | 255.255.255.192 |
| Laptop1 | 192.168.100.3    | 192.168.100.1    | 255.255.255.192 | 
|


### **Routing Table**



### Routingtabelle Router A

| **Destination Network** (Zielnetz + Netzmaske) | **Next Hop** (Nächster Router auf dem Weg zum Ziel) | **Metric** (hier Hop Count)  | **Interface** (auf diesem Router) |
|-----|------|----------|------|
| (A) 192.168.101.0/26   | -- | 0 | e0 |
| (T) 192.168.101.253/30  | -- | 0 | s0 |
| (I) 95.2.50.102/30  | -- | 0 | s1 |
| (B) 192.168.100.0/24  | 192.168.101.254 / 30 | 1 | s0 |
| (Default) 0.0.0.0 / 0 | 95.2.50.101 /30 | -- | s1 |

### Routingtabelle Router B

| **Destination Network**  (Zielnetz + Netzmaske) | **Next Hop** (Nächster Router auf dem Weg zum Ziel) | **Metric** (hier Hop Count)  | **Interface** (auf diesem Router) |
|--------|-------|-------|---------|
| (T) 192.168.101.252/30  | -- | 0 | s0 |
| (B) 192.168.100.0/24  | -- | 0 | e0 |
| (Default) 0.0.0.0 / 0 | 192.168.101.253 | -- | s0 |




## **Cisco-Auftrag:**


#### **Ausgangslage:**  
![](./img/pkt_screen_1.png)


### **Fertigstellung:**
#### **Endprodukt:**
![](./img/pkt_screen_2.png)


#### **Zufälliger ICMP Ping Test**
![](./img/pkt_screen_3.png)
