# **Lerndokumentation N3/2**

### **Ausgangslage**

![](./img/netzwerk_800.png)

**Fehlerbeschrieb für die einzelnen Aufgaben**

#### **Fehlersuche:**
Die Vorgehensweise bei der Fehlersuche war so:

1. Bruteforce Ping mit dem NB-1-1
2. Checken ob die IPs den Hostnamen korrespondierens
4. Gateways überprüfen
5. Router Interfaces prüfen
6. Die Integritäten der IP-Addressen in den Subnetzen überprüfen
7. Routing Tabelle überprüfen


| **Filename** | **Fehlerbeschrieb** |
|--------------|---------------------|
| Netzwerk-Fehler-1.fls | Rechner im Subnetz 4 konnten nicht gepingt werden    |
| Netzwerk-Fehler-1.fls | Das Subnetz 4 kann nicht vom Provider gepingt werden |
| Netzwerk-Fehler-2.fls | Netzwerk 4 5, under der Provider kann man nicht von aussen Erreichen |
| Netzwerk-Fehler-3.fls | Netzwerk 4 5, under der Provider kann man nicht von aussen Erreichen |
| Netzwerk-Fehler-3.fls | Die Subnetze 1 2 3 können nicht vom Provider erreicht werden (ping) |
| Netzwerk-Fehler-4.fls | Die Subntze 1 2 können nicht von aussen erreicht werden |


**Gefundene Fehler (und Korrekturen) für die einzelnen Aufgaben**

| **Filename** | **Gefundener Fehler, ausgeführte Korrektur**    |
|-------------|----|
| Netzwerk-Fehler-1.fls | bei NB-4-1 und NB-4-2 wurde der Gateway 192.168.4.1 nicht eingetragen |
| Netzwerk-Fehler-1.fls | Routing Eintrag im RT-1 eingetragn für das Netzwerk 4|
| Netzwerk-Fehler-2.fls | Der Router mit dem Interface in das Netzwerk 3 war falsch verkabelt. das Kabel wurde entfernt und in das richtige Interface gezogen  |
| Netzwerk-Fehler-2.fls | Der Router RT-2 in hatte die Kabel der Transfernetzwerke vertauscht|
| Netzwerk-Fehler-3.fls | Der Gateway des RT-3 war 192.168.2.1 statt 10.1.0.1  |
| Netzwerk-Fehler-3.fls | Next Gateway für Netzwerk 1 2 3 bei RT-1 war 10.2.1.1 statt 10.2.0.1 |
| Netzwerk-Fehler-3.fls | NIC für Netzwerk 1 2 3 bei RT-1 war 10.2.9.2 statt 10.2.0.2 |
| Netzwerk-Fehler-4.fls | RT-3 ist ein Switch anstatt einen Router, dieser Router wurde wie die Router in den anderen Files konfiguriert. |



