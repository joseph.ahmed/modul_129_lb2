# **Lerndokumentation N2/1**

## **Anforderungen**


| Produkt:   | Site **Global** | Site **Local** | 
|:-----------|:---------------:|:--------------:|
| PCs        | 30              | 8              | 
| Laptops    | 60              | 7              |
| Drucker    | 7               | 2              |
| Router     | 1               | 1
| **Total**  | **98**          | **18**         |


Danach habe ich die Netzwerke nach der Grösse nach geordnet und die Passende CIDR hinzugefügt.


| Netz | Benötigte Adressen | Gewählte  Netzgrösse |
|------|-----------|----------------|
| Netz A | 30 PCs + 60 LTs + 7 Dr + 1 RT + 2 = 100  |  128 |
| Netz B | 8 PCs + 7 LTs + 2 DR + 1 RT + 2 = 20 | 64 |
| Netz T | 0 PCs + 0 LTs + 0 DR + 2 RT + 2 = 4  |   4  |
| Netz I | 0 PCs + 0 LTs + 0 DR + 2 RT + 2 = 4  |   4  |


### **Netzwerk Konzept:**
![](./img/Excel_screen_1.png)


| Netz   | Grösse | Netzadresse/Netzmaske (Bit) | Dezimale Schreibweise der Netzmaske | Broadcastadresse |
|--------|-----|-----|-----|-----|
| Netz I |  4  | 178.19.22.252 /30  | 255.255.255.252 | 178.19.22.255 |
| Netz T |  4  | 178.19.22.248/30   | 255.255.255.252 | 178.19.22.253 |
| Netz A |  80 | 178.19.22.0/25 | 255.255.255.128 | 178.19.22.127 |
| Netz B |  32 | 178.19.22.128/26 | 255.255.255.192 | 178.19.22.160 |



### **Router Konfiguration**

| **Router**      | **Interfaceadresse** | **Interface** |
|-----------------|----------------------|---------------|
| Router A Netz I | 178.19.22.253/30    | s1            |
| Router A Netz A | 178.19.22.1/25       | e0            |
| Router A Netz T | 178.19.22.249/30     | s0            |
| Router B Netz B | 178.19.22.129   /26     | e0            |
| Router B Netz T | 178.19.22.250/30     | s0            |


### **Geräte Konfiiguration**

| -       | IP-Addresse      | Gateway       | Subnetzmaske    | 
| ------- | ---------------- | ------------- | --------------- |
| PC0     | 178.19.22.2      | 178.19.22.1   | 255.255.255.128 |
| Laptop0 | 178.19.22.3      | 178.19.22.1   | 255.255.255.128 |
| PC1     | 178.19.22.130    | 133.95.48.129 | 255.255.255.192 |
| Laptop1 | 178.19.22.131    | 133.95.48.129 | 255.255.255.192 | 
|


### **Routing Table**



### Routingtabelle Router A

| **Destination Network** (Zielnetz + Netzmaske) | **Next Hop** (Nächster Router auf dem Weg zum Ziel) | **Metric** (hier Hop Count)  | **Interface** (auf diesem Router) |
|-----|------|----------|------|
| (A) 178.19.22.0 /25   | -- | 0 | e0 |
| (T) 178.19.22.248 /30  | -- | 0 | s0 |
| (I) 178.19.22.252 /30  | -- | 0 | s1 |
| (B) 178.19.22.128 /26  | 178.19.22.250 / 30 | 1 | s0 |
| (Default) 0.0.0.0 / 0 | 178.19.22.254 /30 | -- | s1 |

### Routingtabelle Router B

| **Destination Network**  (Zielnetz + Netzmaske) | **Next Hop** (Nächster Router auf dem Weg zum Ziel) | **Metric** (hier Hop Count)  | **Interface** (auf diesem Router) |
|--------|-------|-------|---------|
| (T) 178.19.22.248 /30  | -- | 0 | s0 |
| (B) 178.19.22.128 /26  | -- | 0 | e0 |
| (Default) 0.0.0.0 / 0 | 178.19.22.249 | -- | s0 |




## **Cisco-Auftrag:**

Der Auftrag war Grundsätzlich nicht schwer, aber ich habe das Gefühl das pkt_tracer ein bisschen Willkürlich funktioniert. Ich hatte Momente wo ich mich fragte warum etwas nicht funktioniert, aber dann 10min später lief es einwandfrei.

#### **Ausgangslage:**  
![](./img/pkt_screen_1.png)


### **Fertigstellung:**
#### **Endprodukt:**
![](./img/pkt_screen_2.png)


#### **Zufälliger ICMP Ping Test**
![](./img/pkt_screen_3.png)
