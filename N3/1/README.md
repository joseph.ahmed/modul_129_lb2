# **Lerndokumentation N3/1**

### **Ausgangslage**

![](./img/netzwerk_800.png)

**Fehlerbeschrieb für die einzelnen Aufgaben**

#### **Fehlersuche:**
Die Vorgehensweise bei der Fehlersuche war so:

1. Bruteforce Ping mit dem PC_NW1_3
2. Checken ob die IPs den Hostnamen korrespondierens
4. Gateways überprüfen
5. Router Interfaces prüfen
6. Die Integritäten der IP-Addressen in den Subnetzen überprüfen
7. Routing Tabelle überprüfen


| **Filename** | **Fehlerbeschrieb** |
|--------------|---------------------|
| Netzwerk-Fehler-1.fls | PC_NW1_4 kann nicht ausserhalb seines Subnetzes kommunizieren (ping)    |
| Netzwerk-Fehler-2.fls | PC_NW1_4 kann nicht ausserhalb seines Subnetzes kommunizieren (ping)  |
| Netzwerk-Fehler-3.fls | PC_NW1_1 kann nicht gepingt werden |
| Netzwerk-Fehler-4.fls | Netzwerk 3 kann man nicht von aussen Erreichen |
| Netzwerk-Fehler-5.fls | Netzwerk 1 kann man nicht von aussen Erreichen |


**Gefundene Fehler (und Korrekturen) für die einzelnen Aufgaben**

| **Filename** | **Gefundener Fehler, ausgeführte Korrektur**    |
|-------------|----|
| Netzwerk-Fehler-1.fls | Subnetzmaske von PC_NW1_4 ist 255.255.255.0 anstatt 255.255.255.192 |
| Netzwerk-Fehler-2.fls | Gateway von PC_NW1_4 ist ist 192.168.1.2 anstatt 192.168.1.1  |
| Netzwerk-Fehler-3.fls | PC_NW1_1 hat eine APIPA-Adresse anstatt 192.168.1.11 |
| Netzwerk-Fehler-4.fls | Netzmaske beim Eintrag in der Routing Tabelle für das Netzwerk 3 war 255.255.255.252 anstatt 255.255.255.192 |
| Netzwerk-Fehler-5.fls | Die Router im Transfernetz waren falsch vekabelt. und das Kabel wurde von Router zu Router direkt gezogen.|


