# **Lerndokumentation N1/3**

### **Netzwerk Konzept:**
![](./img/Excel_screen_1.png)
![](./img/Excel_screen_2.png)

### **Filius Auftrag:**
#### **Ausgangslage:**
![](./img/Filius_screen_1.png)
#### **Vorgehensweise:**
Zuerst muss man die Router nach dem Konzept oben konfigurieren.

| -           | IP-Addresse   | Subnetzmaske
| ----------- | -----------   | ------------
| Interface 1 | 25.30.120.1   | 255.255.255.240
| Interface 2 | 25.30.120.17  | 255.255.255.240
| Interface 3 | 25.30.120.33  | 255.255.255.240
| Interface 4 | 25.30.120.49  | 255.255.255.240


![](./img/Filius_screen_2.png)

Danach muss ich alle Rechner konfigureieren nach dem Konzept im Excel File Konfigurieren.


| -       | IP-Addresse   | Subnetzmaske    | Gateway
| ------- | ------------- | --------------- | ------------
| PC-2    | 25.30.120.2   | 255.255.255.240 | 25.30.120.1 
| PC-3    | 25.30.120.3   | 255.255.255.240 | 25.30.120.1 
| PC-18   | 25.30.120.18  | 255.255.255.240 | 25.30.120.17
| PC-19   | 25.30.120.19  | 255.255.255.240 | 25.30.120.17 
| PC-34   | 25.30.120.34  | 255.255.255.240 | 25.30.120.33
| PC-35   | 25.30.120.35  | 255.255.255.240 | 25.30.120.33 
| PC-50   | 25.30.120.50  | 255.255.255.240 | 25.30.120.49 
| PC-51   | 25.30.120.51  | 255.255.255.240 | 25.30.120.49

### **Fertigstellung:**


#### **Ping PC-2 - PC-3**
![](./img/Filius_screen_3.png)

#### **Ping PC-2 - PC-51**
![](./img/Filius_screen_4.png)