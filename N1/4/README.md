# **Lerndokumentation N1/4**

### **Netzwerk Konzept:**
Zuerst musste man das Netzwerkkonzept mit dem Excel-File ausfüllen. Die Letzen 2 Subnetze sehen so aus
![](./img/Excel_Screen_1.png)

### **Filius Auftrag:**
#### **Ausgangslage:**
![](./img/filius_screen_1.png)


#### **Fehlersuche:**
Die Vorgehensweise bei der Fehlersuche war so:

1. Subnetzmasken bei den Rechnern Checken
2. Checken ob die IPs den Hostnamen korrespondierens
4. Gateways überprüfen
5. Router Interfaces prüfen
6. Die Integritäten der IP-Addressen in den Subnetzen überprüfen

#### **Fehler:**

|-            | Gefundener Fehler; Korrektur 
|-----------  | ----------------------------
|PC-3        | Gateway falsch falsch im 3. Oktett; Gateway zu 170.11.83.1 (82=>83) geändert.
|PC-10       | IP-Addresse falsch im letzten Oktett; IP-Addresse zu 170.11.83.10 (11=>10) geändert.
|PC-10       | Gateway falsch im letzten Oktett; Gateway zu 170.11.83.9 (8=>9) geändert.
|PC-27       | Gateway falsch im ersten Oktett; Gateway zu 170.11.83.25 (107=>170) geändert.
|PC-43       | kein Gateway eingetragen; Gateway zu 170.11.83.41 geändert.
|PC-50       | IP-Addresse falsch im letzten Oktett; IP-Addresse zu 170.11.83.50 (250=>50) geändert.
|PC-51       | Subnetzmaske ist im 255.255.248.0; Subnetzmaske 255.255.255.248 eingetragen.
|PC-59       | Gateway falsch im letzten Oktett; Gateway zu 170.11.83.57 (59=>57) geändert.
|Interface 4 | IP-Addresse falsch im letzten Oktett; IP-Adresse zu 170.11.83.25 (27=>25) geändert.
|Interface 7 | Subnetzmaske ist 255.255.252.248; Subnetzmaske 255.255.255.248 eingetragen. 


### **Fertigstellung:**


#### **Ping PC-3 - PC-59**

Zum Testen habe ich noch den "PC-59" mit dem "PC-3" gepingt

![](./img/filius_screen_2.png)
