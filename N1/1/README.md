# **Lerndokumentation N1/1**

### **Netzwerk Konzept:**
![](./img/aufgabe_1.png)

### **Filius Auftrag:**
#### **Ausgangslage:**
![](./img/filius_screen_1.png)
#### **Vorgehensweise:**
Zuerst muss man die Router nach dem Konzept oben konfigurieren.


|- | Interface 1 | Interface 2|
|--------|--------|--------|
|IP-Addresse   | 160.160.250.1 | 160.160.250.129|
|Subnetzmaske   | 255.255.255.128 | 255.255.255.128|

![](./img/filius_screen_2.png)

Danach muss ich alle Rechner konfigureieren nach dem Konzept oben Konfigurieren.

|- | PC-01 | PC-02 | PC-11 | PC-12
|--------|--------|--------|--------|--------|
| IP-Addresse   | 160.160.250.2 | 160.160.250.3 | 160.160.250.130 | 160.160.250.131 |
| Subnetzmasek | 255.255.255.128 | 255.255.255.128 | 255.255.255.128 | 255.255.255.128 | 
| Gateway   | 160.160.250.1 | 160.160.250.1 | 160.160.250.129 | 160.160.250.129 |

### **Fertigstellung:**
#### **Endprodukt:**
![](./img/filius_screen_5.png)


#### **Ping PC-01 - PC-02**
![](./img/filius_screen_3.png)

#### **Ping PC-01 - PC-04**
![](./img/filius_screen_4.png)