# **Lerndokumentation N2/1**

### **Netzwerk Konzept:**
![](./img/Excel_screen_1.png)

## **Cisco-Auftrag:**

Der Auftrag war Grundsätzlich nicht schwer, aber ich habe das Gefühl das pkt_tracer ein bisschen Willkürlich funktioniert. Ich hatte Momente wo ich mich fragte warum etwas nicht funktioniert, aber dann 10min später lief es einwandfrei.

#### **Ausgangslage:**  
![](./img/pkt_screen_1.png)

### **Router Konfiguration**

| -               | IP-Addresse      | Subnetzmaske    | Subnetz
| --------------- | ---------------- | --------------- | --------
| FastEthernet0/0 | 133.95.48.1/25   | 255.255.255.128 | GL
| FastEthernet1/0 | 133.95.48.129/25 | 255.255.255.128 | Buchhaltung
| FastEthernet2/0 | 133.95.49.1/25   | 255.255.255.128 | Verkauf
| FastEthernet3/0 | 133.95.49.129/25 | 255.255.255.128 | Einkauf

### **Geräte Konfiiguration**

| -       | IP-Addresse      | Gateway       |Subnetzmaske    | Subnetz
| ------- | ---------------- | ------------- | --------------- | --------
| PC-8050 | 133.95.48.50/25  | 133.95.48.1   | 255.255.255.128 | GL
| PC-8080 | 133.95.48.80/25  | 133.95.48.1   | 255.255.255.128 | GL
| PC-8130 | 133.95.48.130/25 | 133.95.48.129 | 255.255.255.128 | Buchhaltung
| PC_8140 | 133.95.48.140/25 | 133.95.48.129 | 255.255.255.128 | Buchhaltung
| PC-9050 | 133.95.49.50/25  | 133.95.49.1   | 255.255.255.128 | Verkauf
| PC-9080 | 133.95.49.80/25  | 133.95.49.1   | 255.255.255.128 | Verkauf
| PC-9130 | 133.95.49.130/25 | 133.95.49.129 | 255.255.255.128 | Einkauf
| PC_9140 | 133.95.49.140/25 | 133.95.49.129 |v255.255.255.128 | Einkauf


### **Fertigstellung:**
#### **Endprodukt:**
![](./img/pkt_screen_2.png)


#### **Ping PC-8080 - PC-9140**
![](./img/pkt_screen_3.png)
