# **Lerndokumentation N2/1**

## **Anforderungen**

| Produkt:   | GL        | Betrieb | Verkauf | Einkauf | Transfer |
|:-----------|:---------:|:-------:|:-------:|:-------:|:--------:|
| PCs        | 3        | 44       | 6       | 29      | 0        |
| Laptops    | 3        | 36       | 10      | 30      | 0        |
| Drucker    | 2        | 5        | 4       | 6       | 0        |
| Router     | 1        | 1        | 1       | 1       | 2        |
| **Total**  | 9        | 86       | 21      | 66      | 2        |

Danach habe ich die Netzwerke nach der Grösse nach geordnet und die Passende CIDR hinzugefügt.

| -        | CIDR | 
|:--------:|:----:|
| Betrieb  | /25  |
| Einkauf  | /25  |
| Verkauf  | /27  |
| GL       | /28  |
| Transfer | /30  |



### **Netzwerk Konzept:**
![](./img/Excel_screen_2.png)

## **Cisco-Auftrag:**

Der Auftrag war Grundsätzlich nicht schwer, aber ich habe das Gefühl das pkt_tracer ein bisschen Willkürlich funktioniert. Ich hatte Momente wo ich mich fragte warum etwas nicht funktioniert, aber dann 10min später lief es einwandfrei.

#### **Ausgangslage:**  
![](./img/pkt_screen_1.png)

### **Router Konfiguration**

| -               | IP-Addresse      | Subnetzmaske    | Subnetz
| --------------- | ---------------- | --------------- | --------
| FastEthernet0/0 | 133.95.48.1/25   | 255.255.255.128 | GL
| FastEthernet1/0 | 133.95.48.129/25 | 255.255.255.128 | Buchhaltung
| FastEthernet2/0 | 133.95.49.1/25   | 255.255.255.128 | Verkauf
| FastEthernet3/0 | 133.95.49.129/25 | 255.255.255.128 | Einkauf

### **Geräte Konfiiguration**

| -       | IP-Addresse      | Gateway       |Subnetzmaske    | Subnetz
| ------- | ---------------- | ------------- | --------------- | --------
| PC-8050 | 133.95.48.50/25  | 133.95.48.1   | 255.255.255.128 | GL
| PC-8080 | 133.95.48.80/25  | 133.95.48.1   | 255.255.255.128 | GL
| PC-8130 | 133.95.48.130/25 | 133.95.48.129 | 255.255.255.128 | Buchhaltung
| PC_8140 | 133.95.48.140/25 | 133.95.48.129 | 255.255.255.128 | Buchhaltung
| PC-9050 | 133.95.49.50/25  | 133.95.49.1   | 255.255.255.128 | Verkauf
| PC-9080 | 133.95.49.80/25  | 133.95.49.1   | 255.255.255.128 | Verkauf
| PC-9130 | 133.95.49.130/25 | 133.95.49.129 | 255.255.255.128 | Einkauf
| PC_9140 | 133.95.49.140/25 | 133.95.49.129 |v255.255.255.128 | Einkauf


### **Routing Table**


**RT-01**

| Network | Mask    | Next Hop
| ------- | ------- | ---------
| 0.0.0.0 | 0.0.0.0 | 219.60.31.254


**Router-ISP**

| Network | Mask    | Next Hop
| ------- | ------- | ---------
| 219.60.31.0 | 255.255.255.128 | 219.60.31.253


### **Fertigstellung:**
#### **Endprodukt:**
![](./img/pkt_screen_2.png)


#### **Zufälliger ICMP Ping Test**
![](./img/pkt_screen_3.png)
